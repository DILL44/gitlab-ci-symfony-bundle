# source https://gitlab.com/mykiwi/symfony-bootstrapped/blob/master/Makefile
SYMFONY         = $(EXEC_PHP) bin/console
COMPOSER        = $(EXEC_PHP) composer

##
## Project
## -------
##

file: ## Install file (vendor and bundle)
file: bundle

install: ## Install file and db
install: .env migration db

test-quality: ## Install and test
test-quality: bundle lint security deprecation-detector php-cs-fixer

.PHONY: file install test-quality

##
## Files
## -----
##

composer.lock: composer.json
	$(COMPOSER) update

vendor: ## composer update
vendor: composer.lock
	$(COMPOSER) config extra.symfony.allow-contrib true
	$(COMPOSER) update

bundle: ## deploy bundle
bundle:
	$(COMPOSER) config repositories.$(CI_PROJECT_NAMESPACE) vcs ../../$(CI_PROJECT_NAME)
#	$(COMPOSER) require $(CI_PROJECT_PATH) *@dev --no-scripts
	sed -i "N;s/\"require\": {/\"require\": {\n        \"$(CI_PROJECT_NAMESPACE)\/$(CI_PROJECT_NAME)\":\"*@dev\", /g" composer.json
	if [ -d ../Resources/config/ ]; then cp -R ../Resources/config/ .; fi
	composer config extra.symfony.allow-contrib true
	composer update
	#$(COMPOSER) run-script post-update-cmd

.env: ## .env file generate for db
.env:
	@if [ -f .env ]; \
	then\
		sed -i "s/^DATABASE_URL=.*$$/DATABASE_URL=mysql:\/\/root:root@mysql:3306\/db/g" .env; \
	else\
		exit 1; \
	fi

.PHONY: vendor .env bundle

##
## Database
## -----
##

db: ## Reset the database
db: bundle
	-$(SYMFONY) doctrine:database:drop --if-exists --force
	-$(SYMFONY) doctrine:database:create --if-not-exists
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration


migration: ## Generate a new doctrine migration
migration: bundle
	$(SYMFONY) doctrine:migrations:diff

db-validate-schema: ## Validate the doctrine ORM mapping
db-validate-schema: install
	$(SYMFONY) doctrine:schema:validate

# ++ Fixtures

.PHONY: db migration db-validate-schema

##
## Quality assurance
## -----------------
##

lint: ## Lints twig and yaml files
lint: lt ly
lt:
	$(SYMFONY) lint:twig vendor/$(CI_PROJECT_PATH)/Resources/views
ly:
	$(SYMFONY) lint:yaml vendor/$(CI_PROJECT_PATH)/Resources/config

security: ## Check security of your dependencies (https://security.sensiolabs.org/)
security:
	$(COMPOSER) require sensiolabs/security-checker
	$(SYMFONY) security:check

deprecation-detector: ## Check deprecation fonction and class
deprecation-detector:
	deprecation-detector check vendor/$(CI_PROJECT_PATH)

php-cs-fixer: ## php-cs-fixer (http://cs.sensiolabs.org)
php-cs-fixer:
	php-cs-fixer fix  vendor/$(CI_PROJECT_PATH) --rules=@Symfony,@PSR2,@PhpCsFixer,@DoctrineAnnotation --dry-run --using-cache=no --verbose --diff

phpstan: ## scan code
phpstan:
	$(COMPOSER) require phpstan/phpstan phpstan/phpstan-symfony phpstan/phpdoc-parser
	vendor/bin/phpstan analyse -l max -c vendor/$(CI_PROJECT_PATH)/Tests/config/extension.neon vendor/$(CI_PROJECT_PATH)

phpmd: ## scan code with mess detector
phpmd:
	$(COMPOSER) require phpmd/phpmd
	vendor/bin/phpmd vendor/$(CI_PROJECT_PATH) txt vendor/$(CI_PROJECT_PATH)/Tests/config/phprm-unused-test.xml

##
## Tests
## -----
##

config-test-phpunit: ## configure phpunit
config-test-phpunit:
	cp vendor/$(CI_PROJECT_PATH)/Tests/config/phpunit.xml .

config-test-behat: ## install and configure behat
config-test-behat:
	cp vendor/$(CI_PROJECT_PATH)/Tests/config/behat.yml .
	composer require behat/symfony2-extension friends-of-behat/symfony-extension leanphp/behat-code-coverage

phpunit: ## execute php unit test
phpunit: config-test-phpunit
	php bin/phpunit

behat: ## execute behat test
behat: db config-test-behat
	vendor/bin/behat



.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
